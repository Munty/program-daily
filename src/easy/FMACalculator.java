package easy;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;


public class FMACalculator {

	private JFrame frmSimpleFmaCalculator;
	private static JTextField MassField, ForceField, AccelerationField;
	private static int Mass,Force,Acceleration;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FMACalculator window = new FMACalculator();
					window.frmSimpleFmaCalculator.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public FMACalculator() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmSimpleFmaCalculator = new JFrame();
		frmSimpleFmaCalculator.setTitle("Simple F=MA Calculator");
		frmSimpleFmaCalculator.setBounds(100, 100, 313, 213);
		frmSimpleFmaCalculator.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmSimpleFmaCalculator.getContentPane().setLayout(null);
		
		ForceField = new JTextField();
		ForceField.setBounds(45, 12, 86, 20);
		frmSimpleFmaCalculator.getContentPane().add(ForceField);
		ForceField.setColumns(10);
		
		JButton ForceButton = new JButton("Calculate Force");
		ForceButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(MassField.getText()==null||AccelerationField.getText()==null)
				{
					textField.setText("Missing variables, please make sure the Mass and Acceleration fields contain values");
				}
				else
				{
					Mass=Integer.parseInt(MassField.getText());
					Acceleration=Integer.parseInt(AccelerationField.getText());
					Force=Mass*Acceleration;
					ForceField.setText(String.valueOf(Force));
					textField.setText("Force calculation complete");
				}
			}
		});
		ForceButton.setBounds(138, 11, 149, 23);
		frmSimpleFmaCalculator.getContentPane().add(ForceButton);
		
		MassField = new JTextField();
		MassField.setBounds(45, 40, 86, 20);
		frmSimpleFmaCalculator.getContentPane().add(MassField);
		MassField.setColumns(10);
		
		JButton MassButton = new JButton("Calculate Mass");
		MassButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(ForceField.getText()==null||AccelerationField.getText()==null)
				{
					textField.setText("Missing variables, please make sure the Force and Acceleration fields contain values");
				}
				else
				{
					Force=Integer.parseInt(ForceField.getText());
					Acceleration=Integer.parseInt(AccelerationField.getText());
					Mass=Force/Acceleration;
					MassField.setText(String.valueOf(Mass));
					textField.setText("Mass calculation complete");
				}
			}
		});
		MassButton.setBounds(136, 39, 151, 23);
		frmSimpleFmaCalculator.getContentPane().add(MassButton);
		
		AccelerationField = new JTextField();
		AccelerationField.setBounds(45, 68, 86, 20);
		frmSimpleFmaCalculator.getContentPane().add(AccelerationField);
		AccelerationField.setColumns(10);
		
		JButton AccelerationButton = new JButton("Calculate Acceleration");
		AccelerationButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(MassField.getText()==null||ForceField.getText()==null)
				{
					textField.setText("Missing variables, please make sure the Force and Mass fields contain values");
				}
				else
				{
					Mass=Integer.parseInt(MassField.getText());
					Force=Integer.parseInt(ForceField.getText());
					Acceleration=Force/Mass;
					AccelerationField.setText(String.valueOf(Acceleration));
					textField.setText("Acceleration calculation complete");
				}
			}
		});
		AccelerationButton.setBounds(138, 67, 149, 23);
		frmSimpleFmaCalculator.getContentPane().add(AccelerationButton);
		
		textField = new JTextField();
		textField.setBounds(10, 121, 277, 43);
		frmSimpleFmaCalculator.getContentPane().add(textField);
		textField.setColumns(10);
		
		JLabel lblStatus = new JLabel("Status:");
		lblStatus.setBounds(10, 96, 46, 14);
		frmSimpleFmaCalculator.getContentPane().add(lblStatus);
	}
}
