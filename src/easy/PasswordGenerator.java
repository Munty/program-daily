package easy;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;

public class PasswordGenerator {
	
	//maybe create a vector might make it easier, can just append them to the end.
	public static List<String> passlist = new ArrayList<String>();
	
	public static void main(String[] args){
	passlist=mypass(15,10);
	for(int i=0; i<10; i++)
	{
		System.out.println("Password "+(i+1)+":	"+passlist.get(i));
	}
	//passlist.iterator();
	}
	
	private static SecureRandom random = new SecureRandom();
	
	public static List<String> mypass(int passlength, int gennum)
	{
		String s=null;
		int counter=0;
		passlength=passlength*5;
		while(counter<gennum)
		{
		s=new BigInteger(passlength, random).toString(32);
		passlist.add(s);
		counter++;
		}
		
		return passlist;
	}
		 
}
